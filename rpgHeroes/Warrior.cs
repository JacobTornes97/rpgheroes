﻿
namespace rpgHeroes
{
    public class Warrior : Hero
    {
        public Warrior(string name) : base(name)
        {
            LevelAttributes = new HeroAttribute(5, 2, 1);
            ValidWeaponTypes?.Add(WeaponType.WEAPON_AXE);
            ValidWeaponTypes?.Add(WeaponType.WEAPON_HAMMER);
            ValidWeaponTypes?.Add(WeaponType.WEAPON_SWORD);
            
        }

        public override void LevelUp() 
        {
            Level++;
            LevelAttributes?.Increase(new HeroAttribute(3, 2, 1));
        }

        public override void Equip(Weapon weapon)
        {
            if (weapon.RequiredLevel > Level)
            {
                throw new InvalidWeaponException($"Character needs to be level {weapon.RequiredLevel} to equip weapon");
            }

            if (weapon.WeaponType != WeaponType.WEAPON_HAMMER && weapon.WeaponType != WeaponType.WEAPON_SWORD && weapon.WeaponType != WeaponType.WEAPON_AXE)
            {
                throw new InvalidWeaponException($"Character cannot equip a {weapon.WeaponType}");
            }
            Equipment[weapon.Slot] = weapon;
        }

        public override void Equip(Armor armor)
        {
            if (armor.RequiredLevel > Level)
            {
                throw new InvalidArmorException($"Character needs to be level {armor.RequiredLevel} to equip armor");
            }

            if (armor.ArmorType != ArmorType.ARMOR_PLATE && armor.ArmorType != ArmorType.ARMOR_MAIL)
            {
                throw new InvalidArmorException($"Character cannot equip a {armor.ArmorType}");
            }
            Equipment[armor.Slot] = armor;
        }

        public override void Damage()
        {
            //damage is calculated on the fly and not stored
        }

        public override void TotalAttributes()
        {
            //– calculated on the fly and not stored
        }


        public override void Display()
        {
            //• Display – details of Hero to be displayed
        }

    }
}
