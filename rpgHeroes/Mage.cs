﻿

namespace rpgHeroes 
{
    public class Mage : Hero
    {

        public Mage(string name) : base(name)
        {
            this.LevelAttributes = new HeroAttribute(1, 1, 8);
            ValidWeaponTypes?.Add(WeaponType.WEAPON_STAFF);
            ValidWeaponTypes?.Add(WeaponType.WEAPON_WAND); 
            

        }

        public override void LevelUp()
        {
            Level++;
            LevelAttributes?.Increase(new HeroAttribute(1, 1, 5));
        }

        public override void Equip(Weapon weapon)
        {
            if (weapon.RequiredLevel > Level)
            {
                throw new InvalidWeaponException($"Character needs to be level {weapon.RequiredLevel} to equip weapon");
            }

            if (weapon.WeaponType != WeaponType.WEAPON_STAFF && weapon.WeaponType != WeaponType.WEAPON_WAND)
            {
                throw new InvalidWeaponException($"Character cannot equip a {weapon.WeaponType}");
            }
            Equipment[weapon.Slot] = weapon;
        }

        public override void Equip(Armor armor)
        {
            if (armor.RequiredLevel > Level)
            {
                throw new InvalidArmorException($"Character needs to be level {armor.RequiredLevel} to equip armor");
            }

            if (armor.ArmorType != ArmorType.ARMOR_CLOTH)
            {
                throw new InvalidArmorException($"Character cannot equip a {armor.ArmorType}");
            }
            Equipment[armor.Slot] = armor;
        }
        public override void Damage()
        {
            //damage is calculated on the fly and not stored
        }

        public override void TotalAttributes()
        {
            //– calculated on the fly and not stored
        }


        public override void Display()
        {
            //• Display – details of Hero to be displayed
        }





    }
}


