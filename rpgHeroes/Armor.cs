﻿

   namespace rpgHeroes
    {

        public enum ArmorType
        {
            ARMOR_CLOTH,
            ARMOR_LEATHER,
            ARMOR_MAIL,
            ARMOR_PLATE
        }

        public class Armor : Item
        {
            public Armor(string name)
            {
                this.Name = name;
            }
            public ArmorType ArmorType { get; set; }
            public HeroAttribute ArmorAttribute { get; set; }
        }

    }
