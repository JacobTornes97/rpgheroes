﻿

namespace rpgHeroes
{

    public class HeroAttribute
    {
        public int Strength { get; set; }
        public int Dexterity { get; set; }
        public int Intelligence { get; set; }

        public HeroAttribute(int strength, int dexterity, int intelligence)
        {
            Strength = strength;
            Dexterity = dexterity;
            Intelligence = intelligence;
        }

        

        public void Increase (HeroAttribute a)
        {
            Strength += a.Strength;
            Dexterity += a.Dexterity;
            Intelligence += a.Intelligence;
        }
    }
}
