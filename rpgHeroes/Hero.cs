﻿


namespace rpgHeroes
{
    public abstract class Hero
    {
        // constructor with name
        public Hero(string name)
        {
            this.Name = name;
            this.Equipment = new Dictionary<Slot, Item?> {
            {Slot.SLOT_BODY, null},
            {Slot.SLOT_HEADER, null},
            {Slot.SLOT_LEGS, null},
            {Slot.SLOT_WEAPON, null}
          };
        }

        public string Name { get; set; }
        public int Level { get; set; } = 1;
        public HeroAttribute? LevelAttributes { get; set; } = new HeroAttribute(0, 0, 0);
        public Dictionary<Slot, Item?> Equipment { get; set; }

        public List<WeaponType>? ValidWeaponTypes { get; set; }
        public List<ArmorType>? ValidArmorTypes { get; set; }

        public abstract void LevelUp();
        public abstract void Equip(Weapon weapon);
        public abstract void Equip(Armor armor);
        public abstract void Damage();
        public abstract void TotalAttributes();
        public abstract void Display();
    }
}
