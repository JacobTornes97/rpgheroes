﻿

namespace rpgHeroes
{
    public enum Slot
    {
        SLOT_WEAPON,
        SLOT_HEADER,
        SLOT_BODY,
        SLOT_LEGS,
    }
    public abstract class Item
    {
        public string Name { get; set; }
        public int RequiredLevel { get; set; }
        public Slot Slot { get; set; }
    }
}
