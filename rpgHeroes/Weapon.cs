﻿

namespace rpgHeroes
{
        public enum WeaponType
        {
            WEAPON_AXE,
            WEAPON_BOW,
            WEAPON_DAGGER,
            WEAPON_HAMMER,
            WEAPON_STAFF,
            WEAPON_SWORD,
            WEAPON_WAND
        }
        public class Weapon : Item
        {
            public Weapon(string name)
            {
                this.Name = name;
                this.Slot = Slot.SLOT_WEAPON;
            }
            public WeaponType WeaponType { get; set; }
            public int WeaponDamage { get; set; } = 1;
        }
}
