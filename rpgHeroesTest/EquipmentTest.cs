﻿using Microsoft.VisualStudio.TestPlatform.ObjectModel;
using rpgHeroes;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.NetworkInformation;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace rpgHeroesTest
{

    public class EquipementTest
    {


        [Fact(Skip ="Implement this later.")]
        public void Weapon_Creation_Validator()
        {
            //TODO
            // When Weapon is created, it needs to have the correct name, required level, slot,
            // weapon type, and damage
        }

        [Fact(Skip = "Implement this later.")]
        public void Armor_Creation_Validator()
        {   
            //When Armor is created, it needs to have the correct name, required level, slot, 
            //    armor type, and armor attributes
        }

        [Fact]
        public void Equip_Character_With_Invalid_Weapon_Throws_InvalidWeaponException()
        {

            //arrange
            var testAxe = new Weapon("Axe")
            {
                WeaponType = WeaponType.WEAPON_AXE,
                RequiredLevel = 1,
                WeaponDamage = 2,
            };

            var testCharacter = new Mage("Gandalf");

            //act and assert
            Assert.Throws<InvalidWeaponException>(() => { testCharacter.Equip(testAxe);});
        }

        [Fact]
        public void Equip_Character_With_Invalid_Armor_Throws_InvalidArmorException()
        {
            // Arrange
            Armor commonPlateChest = new Armor("Common plate chest")
            {
                ArmorType = ArmorType.ARMOR_PLATE,
                RequiredLevel = 2,
                Slot = Slot.SLOT_BODY,
                ArmorAttribute = new HeroAttribute(6, 1, 1)
            };

            var testCharacter = new Ranger("Aragorn");

            // Act and Assert
            Assert.Throws<InvalidArmorException>(() => { testCharacter.Equip(commonPlateChest); });
        }

        [Fact(Skip = "Implement this later.")]
        public void Total_Attributes_Calculation_Validator()
        {
           //total attributes should be calculated correctly
           //With no equipment
           //With one piece of armor
           //With two pieces of armor
           //With a replaced piece of armor(equip armor, then equip new armor in the same slot)
        }

        [Fact(Skip = "Implement this later.")]
        public void Character_Damage_Calculation_Validator()
        {
            //Hero damage should be calculated properly
            //No weapon equipped
            //Weapon equipped
            //Replaced weapon equipped(equip a weapon then equip a new weapon)
            //Weapon and armor equipped

        }

            
       


    }
}
