
using System;
using System.Threading;
using Xunit;
using rpgHeroes;


namespace rpgHeroesTest
{

    public class HeroCharactersTest
    {


        [Fact]
        public void Mage_Level_Initializes_Correctly()
        {
            //Check for level
            // Arrange
            Mage mage = new("Mage");
            List<Hero> heroes = new List<Hero>();
            heroes.Add(mage);
            int expected = 1;
            // Act
            int actual = heroes[0].Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Mage_Attributes_Initializes_Correctly()
        {
            //Check for attributes
            Mage mage = new("Mage");
            HeroAttribute expected = new HeroAttribute(1, 1, 8);
            //Act
            HeroAttribute? actual = mage.LevelAttributes;
            //Assert
            Assert.Equal(expected.Intelligence, actual.Intelligence);
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);



        }

        [Fact]
        public void Character_Mage_Initializes_Correctly()
        {
            //check for name
            // Arrange
            string expectedName = "Mage";

            // Act
            Mage myMage = new Mage(expectedName);

            // Assert
            Assert.Equal(expectedName, myMage.Name);
        }


        [Fact]
        public void Character_Ranger_Level_Initializes_Correctly()
        {
            //Check for level
            // Arrange
            Ranger ranger = new("Ranger");
            int expected = 1;
            // Act
            int actual = ranger.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Character_Ranger_Attributes_Initializes_Correctly()
        {
            //Check for attributes
            Ranger ranger = new("Ranger");
            HeroAttribute expected = new HeroAttribute(1, 7, 1);
            //Act
            HeroAttribute? actual = ranger.LevelAttributes;
            //Assert
            Assert.Equal(expected.Intelligence, actual.Intelligence);
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);



        }

        [Fact]
        public void Character_Ranger_Initializes_Correctly()
        {
            //check for name
            // Arrange
            string expectedName = "Ranger";

            // Act
            Ranger myRanger = new Ranger(expectedName);

            // Assert
            Assert.Equal(expectedName, myRanger.Name);
        }


        [Fact]
        public void Character_Rogue_Level_Initializes_Correctly()
        {
            //Check for level
            // Arrange
            Rogue rogue = new("Rogue");
            int expected = 1;
            // Act
            int actual = rogue.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Character_Rogue_Attributes_Initializes_Correctly()
        {
            //Check for attributes
            Rogue rogue = new("Rogue");
            HeroAttribute expected = new HeroAttribute(2, 6, 1);
            //Act
            HeroAttribute? actual = rogue.LevelAttributes;
            //Assert
            Assert.Equal(expected.Intelligence, actual.Intelligence);
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);



        }

        [Fact]
        public void Character_Rogue_Initializes_Correctly()
        {
            //check for name
            // Arrange
            string expectedName = "Rogue";

            // Act
            Rogue myRogue = new Rogue(expectedName);

            // Assert
            Assert.Equal(expectedName, myRogue.Name);
        }


        [Fact]
        public void Character_Warrior_Level_Initializes_Correctly()
        {
            //Check for level
            // Arrange
            Warrior warrior = new("Warrior");
            int expected = 1;
            // Act
            int actual = warrior.Level;
            // Assert
            Assert.Equal(expected, actual);
        }

        [Fact]
        public void Character_Warrior_Attributes_Initializes_Correctly()
        {
            //Check for attributes
            Warrior warrior = new("Warrior");
            HeroAttribute expected = new HeroAttribute(5, 2, 1);
            //Act
            HeroAttribute? actual = warrior.LevelAttributes;
            //Assert
            Assert.Equal(expected.Intelligence, actual.Intelligence);
            Assert.Equal(expected.Strength, actual.Strength);
            Assert.Equal(expected.Dexterity, actual.Dexterity);



        }

        [Fact]


        public void Character_Warrior_Initializes_Correctly()
        {
            //check for name
            // Arrange
            string expectedName = "Gandalf";

            // Act
            Warrior myWarrior = new Warrior(expectedName);

            // Assert
            Assert.Equal(expectedName, myWarrior.Name);
        }

        [Fact]
        public void Character_Display_Validator()
        {
            //Heroes should display their state correctly
        }

        [Fact]

        public void Mage_LevellingUp_Attribute_Increment_Validator()
        {

            //Arrange

            Mage mage = new Mage("Gandalf");
            var initialStrength = mage.LevelAttributes?.Strength;
            var initialDexterity = mage.LevelAttributes?.Dexterity;
            var initialIntelligence = mage.LevelAttributes?.Intelligence;
            
            //Act
            mage.LevelUp();

            var newStrength = mage.LevelAttributes?.Strength;
            var newDexterity = mage.LevelAttributes?.Dexterity;
            var newIntelligence = mage.LevelAttributes?.Intelligence;

            //Assert
            Assert.Equal(newStrength - initialStrength, 1);
            Assert.Equal(newDexterity - initialDexterity, 1);
            Assert.Equal(newIntelligence - initialIntelligence, 5);

        }

        [Fact]
        public void Ranger_LevellingUp_Attribute_Increment_Validator()
        {

            //Arrange

            Ranger ranger = new Ranger("Ranger");
            var initialStrength = ranger.LevelAttributes?.Strength;
            var initialDexterity = ranger.LevelAttributes?.Dexterity;
            var initialIntelligence = ranger.LevelAttributes?.Intelligence;

            //Act
            ranger.LevelUp();

            var newStrength = ranger.LevelAttributes?.Strength;
            var newDexterity = ranger.LevelAttributes?.Dexterity;
            var newIntelligence = ranger.LevelAttributes?.Intelligence;

            //Assert
            Assert.Equal(newStrength - initialStrength, 1);
            Assert.Equal(newDexterity - initialDexterity, 5);
            Assert.Equal(newIntelligence - initialIntelligence, 1);

        }

        [Fact]
        public void Rogue_LevellingUp_Attribute_Increment_Validator()
        {

            //Arrange
            
            Rogue rogue = new Rogue("Rogue");
            var initialStrength = rogue.LevelAttributes?.Strength;
            var initialDexterity = rogue.LevelAttributes?.Dexterity;
            var initialIntelligence = rogue.LevelAttributes?.Intelligence;

            //Act
            rogue.LevelUp();

            var newStrength = rogue.LevelAttributes?.Strength;
            var newDexterity = rogue.LevelAttributes?.Dexterity;
            var newIntelligence = rogue.LevelAttributes?.Intelligence;

            //Assert
            Assert.Equal(newStrength - initialStrength, 1);
            Assert.Equal(newDexterity - initialDexterity, 4);
            Assert.Equal(newIntelligence - initialIntelligence, 1);

        }

        [Fact]
        public void Warrior_LevellingUp_Attribute_Increment_Validator()
        {

            //Arrange

            Warrior warrior = new Warrior("Warrior");
            var initialStrength = warrior.LevelAttributes?.Strength;
            var initialDexterity = warrior.LevelAttributes?.Dexterity;
            var initialIntelligence = warrior.LevelAttributes?.Intelligence;

            //Act
            warrior.LevelUp();

            var newStrength = warrior.LevelAttributes?.Strength;
            var newDexterity = warrior.LevelAttributes?.Dexterity;
            var newIntelligence = warrior.LevelAttributes?.Intelligence;

            //Assert
            Assert.Equal(newStrength - initialStrength, 3);
            Assert.Equal(newDexterity - initialDexterity, 2);
            Assert.Equal(newIntelligence - initialIntelligence, 1);

        }

        [Fact]
        public void Check_If_HeroAttribute_Increases_Correctly()
        {
          
                // Arrange
                var heroAttribute = new HeroAttribute(10, 20, 30);

                // Act
                heroAttribute.Strength += 2;
                heroAttribute.Dexterity += 2;
                heroAttribute.Intelligence += 2;

                // Assert
                Assert.Equal(12, heroAttribute.Strength);
                Assert.Equal(22, heroAttribute.Dexterity);
                Assert.Equal(32, heroAttribute.Intelligence);
   
        }

    }






}