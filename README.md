# RPG Heroes 
A console application written in C#. 

## Introduction

This repo was created as part of an assignment for a C# course.

Not all parts was finished within the deadline. C# had a lot of new concepts that I did not know of :-)

### Stuff that has been done

Most of the classes and class hierarchy structure and exceptions have been implemented enough that we can equip the Heroes with armor and weapons.

There are also 20 unit tests implemented, to cover some of the creation and equip functionality.

I also added a CI yml file for GitLab so that tests will run on each push.

## Heroes
In the game there are currently character types
- Mage
- Ranger
- Rogue
- Warrior

### Hero Attributes
- Strength
- Dexterity
- Intelligence

# Equipment
A hero can equip different types of Armor and Weapons

## Damage
A character can deal damage based on it's type, primary attributes and equipped weapon and armor.

